import Point from "./point";

interface IEllipse {
    center: Point;
    radius1: number;
    radius2: number;
    isCircle(): boolean;
}

export default class Ellipse implements IEllipse {
    center: Point;
    radius1: number;
    radius2: number;

    constructor(center: Point, radius1: number, radius2: number) {
        this.center = center;
        this.radius1 = radius1;
        this.radius2 = radius2;
    }

    isCircle() {
        return this.radius1 === this.radius2;
    }

    contains(point: Point): boolean {
        return (point.x - this.center.x) ** 2 / this.radius1 ** 2 + (point.y - this.center.y) ** 2 / this.radius2 ** 2 < 1;
    }

    get area(): number {
        return Math.PI * this.radius1 * this.radius2;
    }
}
