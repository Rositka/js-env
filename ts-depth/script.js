import * as Field from './field.js'
import * as Rectangle from './rectangle.js';
import * as Ellipse from './ellipse.js';
import * as Point from './point.js';

const field = new Field(document.getElementById('canvas').getContext('2d'));

const rectangle1 = new Rectangle(
    Point.randomPoint(0, Field.WIDTH, 0, Field.HEIGHT),
    Point.randomPoint(0, Field.WIDTH, 0, Field.HEIGHT)
);
const rectangle2 = new Rectangle(
    Point.randomPoint(0, Field.WIDTH, 0, Field.HEIGHT),
    Point.randomPoint(0, Field.WIDTH, 0, Field.HEIGHT)
);

field.strokeRectangle(rectangle1);
field.strokeRectangle(rectangle2);

const rectangle = rectangle2.intersect(rectangle1);

if (rectangle) {
    field.strokeEllipse(rectangle);

    let array = [];

    let radiusX = (rectangle.right - rectangle.left) / 2;
    let radiusY = (rectangle.top - rectangle.bottom) / 2;
    let ellipse = new Ellipse(
        new Point(rectangle.left + radiusX, rectangle.bottom + radiusY),
        radiusX,
        radiusY
    );

    while (array.length < 1000) {
        let point = Point.randomPoint(
            rectangle.left,
            rectangle.right,
            rectangle.bottom,
            rectangle.top
        );

        if (ellipse.contains(point)) {
            array.push(point);
        }
    }

    array.forEach(point => field.fillDot(point));
}
