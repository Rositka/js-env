"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Ellipse = void 0;
var Ellipse = /** @class */ (function () {
    function Ellipse(center, radius1, radius2) {
        this.center = center;
        this.radius1 = radius1;
        this.radius2 = radius2;
    }
    Ellipse.prototype.isCircle = function () {
        return this.radius1 === this.radius2;
    };
    Ellipse.prototype.contains = function (point) {
        return Math.pow((point.x - this.center.x), 2) / Math.pow(this.radius1, 2) + Math.pow((point.y - this.center.y), 2) / Math.pow(this.radius2, 2) < 1;
    };
    Object.defineProperty(Ellipse.prototype, "area", {
        get: function () {
            return Math.PI * this.radius1 * this.radius2;
        },
        enumerable: false,
        configurable: true
    });
    return Ellipse;
}());
exports.Ellipse = Ellipse;
