"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Line = void 0;
var Line = /** @class */ (function () {
    function Line(start, end) {
        this.start = start;
        this.end = end;
    }
    Line.prototype.hypot = function () {
        var values = [];
        for (var _i = 0; _i < arguments.length; _i++) {
            values[_i] = arguments[_i];
        }
        return 0;
    };
    Object.defineProperty(Line.prototype, "length", {
        get: function () {
            return Math.hypot(this.start.x - this.end.x, this.start.y - this.end.y);
        },
        enumerable: false,
        configurable: true
    });
    return Line;
}());
exports.Line = Line;
