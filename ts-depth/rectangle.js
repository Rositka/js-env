"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Rectangle = void 0;
var point_1 = require("./point");
var line_1 = require("./line");
var Rectangle = /** @class */ (function () {
    function Rectangle(point1, point2) {
        this.point1 = point1;
        this.point2 = point2;
    }
    Rectangle.prototype.isSquare = function () {
        var lb = new point_1.Point(this.left, this.bottom);
        var lt = new point_1.Point(this.left, this.top);
        var rb = new point_1.Point(this.right, this.bottom);
        return new line_1.Line(lb, lt).length === new line_1.Line(lb, rb).length;
    };
    Rectangle.prototype.contains = function (point) {
        return point.x > this.left &&
            point.x < this.right &&
            point.y > this.bottom &&
            point.y < this.top;
    };
    Object.defineProperty(Rectangle.prototype, "left", {
        get: function () {
            return Math.min(this.point1.x, this.point2.x);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Rectangle.prototype, "right", {
        get: function () {
            return Math.max(this.point1.x, this.point2.x);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Rectangle.prototype, "bottom", {
        get: function () {
            return Math.min(this.point1.y, this.point2.y);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Rectangle.prototype, "top", {
        get: function () {
            return Math.max(this.point1.y, this.point2.y);
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Rectangle.prototype, "width", {
        get: function () {
            return this.right - this.left;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Rectangle.prototype, "height", {
        get: function () {
            return this.top - this.bottom;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Rectangle.prototype, "area", {
        get: function () {
            return this.width * this.height;
        },
        enumerable: false,
        configurable: true
    });
    Rectangle.prototype.intersect = function (rectangle) {
        var left = Math.max(rectangle.left, this.left), right = Math.min(rectangle.right, this.right), top = Math.min(rectangle.top, this.top), bottom = Math.max(rectangle.bottom, this.bottom);
        if (right > left && top > bottom) {
            return new Rectangle(new point_1.Point(left, bottom), new point_1.Point(right, top));
        }
    };
    return Rectangle;
}());
exports.Rectangle = Rectangle;
