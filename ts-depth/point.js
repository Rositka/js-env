"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Point = void 0;
var Point = /** @class */ (function () {
    function Point(x, y) {
        this.x = x;
        this.y = y;
    }
    Point.randomNumber = function (from, to) {
        return Math.random() * (to - from) + from;
    };
    Point.randomPoint = function (xFrom, xTo, yFrom, yTo) {
        return new Point(Point.randomNumber(xFrom, xTo), Point.randomNumber(yFrom, yTo));
    };
    return Point;
}());
exports.Point = Point;
