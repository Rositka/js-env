import Point from './point';
import Line from './line';

interface IRectangle {
    point1: Point;
    point2: Point;
    isSquare(): boolean;
}

type newRectangle = {
    left: number;
    right: number;
    top: number;
    bottom: number;
}

export default class Rectangle implements IRectangle {
    point1: Point;
    point2: Point;

    constructor(point1: Point, point2: Point) {
        this.point1 = point1;
        this.point2 = point2;
    }

    isSquare() {
        const lb = new Point(this.left, this.bottom);
        const lt = new Point(this.left, this.top);
        const rb = new Point(this.right, this.bottom);
        return new Line(lb, lt).length === new Line(lb, rb).length;
    }

    contains(point: Point): boolean {
        return point.x > this.left &&
            point.x < this.right &&
            point.y > this.bottom &&
            point.y < this.top;
    }

    get left(): number {
        return Math.min(this.point1.x, this.point2.x);
    }

    get right(): number {
        return Math.max(this.point1.x, this.point2.x);
    }

    get bottom(): number {
        return Math.min(this.point1.y, this.point2.y);
    }

    get top(): number {
        return Math.max(this.point1.y, this.point2.y);
    }

    get width(): number {
        return this.right - this.left;
    }

    get height(): number {
        return this.top - this.bottom;
    }

    get area(): number {
        return this.width * this.height;
    }

    intersect(rectangle: newRectangle): newRectangle {
        const
            left = Math.max(rectangle.left, this.left),
            right = Math.min(rectangle.right, this.right),
            top = Math.min(rectangle.top, this.top),
            bottom = Math.max(rectangle.bottom, this.bottom);

        if (right > left && top > bottom) {
            return new Rectangle(new Point(left, bottom), new Point(right, top));
        }
    }
}
