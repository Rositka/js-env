"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.Field = void 0;
var Field = /** @class */ (function () {
    function Field(ctx) {
        this.ctx = ctx;
    }
    Object.defineProperty(Field, "WIDTH", {
        get: function () {
            return 800;
        },
        enumerable: false,
        configurable: true
    });
    Object.defineProperty(Field, "HEIGHT", {
        get: function () {
            return 500;
        },
        enumerable: false,
        configurable: true
    });
    Field.prototype.strokeRectangle = function (rectangle) {
        this.ctx.strokeStyle = '#595959';
        this.ctx.lineWidth = 2;
        this.ctx.strokeRect(rectangle.left, rectangle.top, rectangle.width, -rectangle.height);
    };
    Field.prototype.fillDot = function (point) {
        this.ctx.beginPath();
        this.ctx.arc(point.x, point.y, 4, 0, 2 * Math.PI);
        this.ctx.fillStyle = 'red';
        this.ctx.fill();
    };
    Field.prototype.strokeEllipse = function (rectangle) {
        var x = rectangle.left + rectangle.width / 2;
        var y = rectangle.bottom + rectangle.height / 2;
        this.ctx.beginPath();
        this.ctx.ellipse(x, y, rectangle.width / 2, rectangle.height / 2, Math.PI, 0, 2 * Math.PI);
        this.ctx.stroke();
    };
    return Field;
}());
exports.Field = Field;
