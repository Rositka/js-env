const task = graph => {

    const findLetterInGraph = (input, obj = {}) => {
        const {letter, children} = input;

        let {[letter]: value} = obj;
        const newObj = {...obj, [letter]: ++value || 1};

        return ('children' in input) ?
            children.reduce((prev, item) => {
                return findLetterInGraph(item, prev);
            }, newObj)
            : newObj;
    }

    const stringOfLetter = Object.entries(findLetterInGraph(graph))
        .sort((a, b) => a[0].localeCompare(b[0]))
        .sort((a, b) => (b[1] - a[1]))
        .map(item => item[0])
        .join('');

    return stringOfLetter;
};

console.log(task(graph));


