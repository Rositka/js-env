/*
    Дано граф вузли якого представлені обєктами
        Node = {
            value: number,
            children: Node[]
        }
    Знайти різницю максимального та мінімального значень поля `value`.
 */

const {graph} = require('./graph');

const minMaxDiff = graph => {
    // TODO: Place your solution here!
};

const checks = [
    minMaxDiff({value: 1}) === 0, // 1 - 1
    minMaxDiff({value: 1, children: [{value: 2}]}) === 1, // 2 - 1
    minMaxDiff({value: 1, children: [{value: 2}, {value: 3}]}) === 2, // 3 - 1
    minMaxDiff({value: 1, children: [{value: 2, children: [{value: 4}]}, {value: 3}]}) === 3, // 4 - 1
    minMaxDiff(graph) === 100
];

console.group('Task 04');
console.log(checks);
console.log(checks.every(i => i) ? 'Everything done! ;)' : 'Something goes wrong :\'(');
console.groupEnd();

module.exports = {checks};
