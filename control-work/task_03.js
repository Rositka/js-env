/*
    Дано граф вузли якого представлені обєктами
        Node = {
            value: number,
            children: Node[]
        }
    Знайти максимальну глибину дерева - довжину найдовшої гілки, рахуючи корінь за 0
 */

const {graph} = require('./graph');

const depth = graph => {
    let max = 0;
    const findMaxInGraph = (input, count = 0) => {

        if ('children' in input) {
            count++;
            return input.children.reduce((prev, item) => {
                return findMaxInGraph(item, prev);
            }, count)
        }

        if (count > max) max = count;
        return 0;
    }
    findMaxInGraph(graph);
    return max;
};

const checks = [
    depth({value: 1}) === 0,
    depth({value: 1, children: [{value: 2}]}) === 1,
    depth({value: 1, children: [{value: 2}, {value: 3}]}) === 1,
    depth({value: 1, children: [{value: 2, children: [{value: 4}]}, {value: 3}]}) === 2,
    depth(graph) === 10
];

console.group('Task 03');
console.log(checks);
console.log(checks.every(i => i) ? 'Everything done! ;)' : 'Something goes wrong :\'(');
console.groupEnd();

module.exports = {checks};
