let idCount = 1;

export default class DataTable {
    #parent;
    #data;
    #arrOfOption = [5, 10, 25, 50, 100, 250];
    #switchSort = ['', 1];
    #keys;
    #clone;

    constructor(parent) {
        this.#parent = parent;
    }

    set data(data) {
        this.#data = data;
        this.#keys = Object.keys(data[0]);
        this.#clone = this.#data.map(a => Object.assign({}, a));
    }

    static get createTable() {
        const table = document.createElement('table');
        table.setAttribute('id', `${idCount++}`);
        table.className = 'table';
        document.body.append(table);
        return table;
    }

    #createThead(parent, arrData) {
        const thead = document.createElement('thead');
        const row = document.createElement('tr');
        parent.append(thead);
        thead.append(row);
        this.#switchSort[0] = arrData[0];
        arrData.forEach(item => {
            let th = document.createElement('th');
            th.textContent = `${item}`;
            row.append(th);
            let span1 = document.createElement('span');
            th.append(span1);
            let span2 = document.createElement('span');
            th.append(span2);
        });
        return row;
    }

    #renderSortButton(parent) {
        parent.childNodes.forEach(item => {
            item.firstElementChild.textContent = "▼";
            item.lastElementChild.textContent = "▲";

            if (item.firstChild.textContent === this.#switchSort[0]) {
                if(this.#switchSort[1] < 0) item.firstElementChild.textContent = "";
                else if(this.#switchSort[1] > 0) item.lastElementChild.textContent = "";
            }
        })
    }

    #createRows(parent, initialValue, page, value) {
        for (let i = 0; i < initialValue; i++) {
            let current = i + initialValue * (page - 1);
            if (current < this.#data.length) {
                const row = document.createElement('tr');
                parent.append(row);
                this.#keys.forEach(() => {
                    let td = document.createElement('td');
                    row.append(td);
                })
                row.childNodes.forEach((item, index) => {
                    item.textContent = value[current][`${this.#keys[index]}`];
                })
            }
        }
    }

    #createTbody(parent, initialValue, page, value) {
        const tbody = document.createElement('tbody');
        tbody.setAttribute('id', `${idCount++}`);
        parent.append(tbody);
        this.#createRows(tbody, initialValue, page, value);
        return idCount - 1;
    }

    #createOptions(parent) {
        for (let i = 0; i < this.#arrOfOption.length; i++) {
            let option = document.createElement('option');
            option.setAttribute('value', `${this.#arrOfOption[i]}`);
            option.textContent = `${this.#arrOfOption[i]}`;
            parent.append(option);
            const selectedValue = document.querySelectorAll('option');
            for (let j = 0; j < selectedValue.length; j++) {
                if (selectedValue[j].value === '10') selectedValue[j].selected = true;
            }
        }
    }

    static replaceTbody(idTable, idBody, initialValue, page, thisContext) {
        const table = document.getElementById(idTable);
        const tbodyOld = document.getElementById(idBody);
        tbodyOld.remove();
        const tbodyNew = document.createElement('tbody');
        tbodyNew.setAttribute('id', idBody);
        table.append(tbodyNew);
        thisContext.#createRows(tbodyNew, initialValue, page, thisContext.#clone);
    }

    static get createBlock() {
        const ul = document.createElement('ul');
        ul.className = 'list';
        document.body.append(ul);
        return ul;
    }

    static createListItems(parent) {
        for (let i = 0; i < 7; i++) {
            let li = document.createElement('li');
            li.className = 'list-item';
            parent.append(li);
        }
    }

    static createPagination(list, currentPage, allPages) {
        let lengthList = list.length;
        if (lengthList >= allPages) {
            for (let i = 0; i < lengthList; i++)
                if (i < allPages) list[i].textContent = `${i + 1}`;
                else list[i].textContent = '';
            }
        else {
            for (let i = 0; i < lengthList; i++) {
                if (currentPage < lengthList - 2) {
                    if (i === lengthList - 2) list[i].textContent = '...';
                    else if (i === lengthList - 1) list[i].textContent = allPages;
                    else list[i].textContent = `${i + 1}`;
                } else if (allPages - currentPage < lengthList - 3) {
                    if (i === 1) list[i].textContent = '...';
                    else if (i === 0) list[i].textContent = 1;
                    else list[i].textContent = allPages + i - (lengthList - 1);
                } else {
                    if (i === 0) list[i].textContent = 1;
                    else if (i === lengthList - 1) list[i].textContent = allPages;
                    else if (i === 1 || i === lengthList - 2) list[i].textContent = '...';
                    else list[i].textContent = currentPage - Math.floor((lengthList - 1) / 2) + i;
                }
            }
        }
    }

    #sortTable() {
        const dir = this.#switchSort[1];
        const key = this.#switchSort[0];
        this.#clone = this.#data.map(a => Object.assign({}, a));
        if (!isNaN(this.#clone[0][key])) {
            this.#clone.sort((a, b) => (a[key] - b[key]) * dir);
        } else if (new Date(this.#clone[0][key])) {
            if (dir > 0) this.#clone.sort((a, b) => new Date(a[key]) - new Date(b[key]));
            else this.#clone.sort((a, b) => new Date(b[key]) - new Date(a[key]));
        } else {
            if (dir > 0) this.#clone.sort((a, b) => a[key].localeCompare(b[key]));
            //else this.#clone.sort((a, b) => a[key] < b[key]);
        }
    }

    build() {
        const context = this;
        let rows = 10;
        let currentPage = 1;
        let allPages = Math.ceil(this.#data.length / rows);

        const table = DataTable.createTable;
        const tr = this.#createThead(table, this.#keys);
        this.#renderSortButton(tr);
        const idTbody = this.#createTbody(table, rows, currentPage, this.#clone);

        const select = document.createElement('select');
        select.setAttribute('onchange', 'this.options[this.selectedIndex].value');
        select.className = 'select';
        document.body.append(select);

        const ul = DataTable.createBlock;
        DataTable.createListItems(ul);
        const pagList = ul.childNodes;
        DataTable.createPagination(pagList, currentPage, allPages);
        let spanList = [];
        tr.childNodes.forEach(item => {
            spanList.push(item.firstElementChild);
            spanList.push(item.lastElementChild);
        });

        select.addEventListener('change', function (e) {
            rows = Array.from(select.options)
                .filter(option => option.selected)
                .map(option => Number(option.value))[0];

            allPages = Math.ceil(context.#data.length / rows);
            if (currentPage > allPages) currentPage = allPages;
            DataTable.replaceTbody(idTbody - 1, idTbody, rows, currentPage, context);
            DataTable.createPagination(pagList, currentPage, allPages);
        });
        this.#createOptions(select);

        for (let item of pagList) {
            item.addEventListener('click', function(e) {
                let clickValue = +this.textContent;
                if (clickValue !== currentPage) {
                    if (isNaN(clickValue)) {
                        if (this.previousSibling.textContent > 1) {
                            currentPage = Math.ceil((currentPage + allPages) / 2);
                        } else currentPage = Math.ceil((currentPage - 4) / 2);
                    } else currentPage = clickValue;

                    DataTable.replaceTbody(idTbody - 1, idTbody, rows, currentPage, context);
                    DataTable.createPagination(pagList, currentPage, allPages);
                }
            })
        }

        for (let item of spanList) {
            item.addEventListener('click', function(e) {
                context.#switchSort[0] = this.parentNode.firstChild.textContent;
                if (this.textContent === "▼")  context.#switchSort[1] = -1;
                else if (this.textContent === "▲")  context.#switchSort[1] = 1;

                context.#renderSortButton(tr);
                context.#sortTable();
                DataTable.replaceTbody(idTbody - 1, idTbody, rows, currentPage, context);
            })
        }
    }
}
