const SCALE = 5;

const BLACK_STROKE = {
    stroke: '#000000',
    'stroke-width': 0.3,
};

const post = 'Software Engineer';
const languages = ['JavaScript', 'Java', 'PHP'];
const color = ['#FFD700', '#1E90FF', '#8A2BE2', '#C71585'];

const min = item => item <= 900;
const middle = item => item > 900 && item <= 1500;
const aboveMiddle = item => item > 1500 && item < 3000;
const high = item => item >= 3000;
const range = [min, middle, aboveMiddle, high];

const salariesAll = languages.map(lang => data.filter(item => item[4] === post && item[7] === lang)
    .map(item => item[2]));

const salariesArr = arr => range.map(item => arr.filter(item).length);
const salariesCategory = salariesAll.map(item => salariesArr(item));

const  percentArr = arr => {
    let total = arr.reduce((prev, item) => prev + item, 0);
    return arr.map(item => Math.round(item * 1000 / total) / 10);
};
const percentSal = salariesCategory.map(item => percentArr(item));

//visualisation
const createElement = (determeName, parentBlock) => {
    const element = document.createElementNS('http://www.w3.org/2000/svg', determeName);
    parentBlock.appendChild(element);
    return element;
};

const setSvgAttributes = (block, ...attr) => {
    attr.forEach(item => {
        Object.entries(item).forEach(([key, value]) => {
            block.setAttribute(key, value);
        });
    });
};

// visualisation
const svg = createElement('svg', document.body);
setSvgAttributes(svg, {width: 200 * SCALE, height: 200 * SCALE, viewBox: '0 0 200 200'});

const g = createElement('g', svg);

let arrRect = [];
let step = 32;
let x = 50;
for (let i = 0; i < range.length; i++) {
    arrRect[i] = {
        id: 'rect' + i,
        x: x,
        y: '22',
        fill: color[i],
        width: '3',
        height: '3'
    };
    x += step;
    let rectId = createElement('rect', g);
    setSvgAttributes(rectId, arrRect[i]);
}

let chartBar = [[],[],[]];
let y = 10;
for (let i = 0; i < languages.length; i++) {
    let x = 50;
    y += 20;

    for (let j = 0; j < range.length; j++) {
        chartBar[i][j] = {
            id: 'chart' + i + '_' + j,
            x: x,
            y: y,
            fill: color[j],
            width: percentSal[i][j],
            height: '15'
        };
        x += percentSal[i][j];

        let  chartId = createElement('rect', g);
        setSvgAttributes(chartId, chartBar[i][j]);
    }
}

let arrLines = [];
let stepLine = 20;
let x1 = 50;
let x2 = 50;
for (let i = 0; i < 6; i++) {
    arrLines[i] = {
        id: 'line' + i,
        x1: x1,
        x2: x2,
        y1: '100',
        y2: '96',
        stroke: 'black',
        'stroke-width': 0.3
    };
    x1 += stepLine;
    x2 += stepLine;
    let lineId = createElement('line', g);
    setSvgAttributes(lineId, arrLines[i]);
}
console.log(arrLines);

const line = createElement('line', g);
setSvgAttributes(line, {id: 'line', x1: '50', x2: '150', y1: '98', y2: '98', stroke: 'black'}, BLACK_STROKE);

let infoBlock = document.getElementById('info');
svg.append(infoBlock);

let langBlock = document.getElementById('languages');
svg.append(langBlock);

let percentBlock = document.getElementById('percentage');
svg.append(percentBlock);
