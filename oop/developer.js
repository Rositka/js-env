import Person from './person.js';
import Validator from "./validator.js";

class Developer extends Person {
    constructor(firstName, lastName) {
        Validator.validateInputArgument(firstName, 'firstName');
        Validator.validateInputArgument(lastName, 'lastName');

        super(firstName, lastName);
        this.assignedTasks = [];
    }

    static get DAILY_EFFICIENCY() {
        return 330;
    }

    get fullNme() {
        return this.firstName + " " + this.lastName;
    }

    assign(task) {
        Validator.validateExistenceArgument(task, 'assign');
        task.assign(this);
    }

}

export default Developer;

