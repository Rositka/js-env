import Developer from "./developer.js";
import Validator from "./validator.js";

export default class Release {
    constructor(name, description, tasks) {
        Validator.validateInputArgument(name, 'release name');
        Validator.validateInputArgument(description, 'description');
        Validator.validateArray(tasks, 'tasks');

        this.name = name;
        this.description = description;
        this.tasks = tasks;
    }

    addTasks(task) {
        Validator.validateExistenceArgument(task, 'addTasks');
        this.tasks.push(task);
    }

    assignRelease(date) {
        Validator.validateNewDate(date, 'new Date');
        this.releaseDate = date;
    }

    get allCalendarDays() {
        const remainTime = this.tasks.reduce((prev, item) => prev + item.remainTime, 0)
        const workDays =  Math.ceil(remainTime / Developer.DAILY_EFFICIENCY);
        return Math.floor(workDays / 5) * 7 + workDays % 5 + 7;
    }

    static get ONE_DAY() {
        return 24 * 3600 * 1000;
    }

    offsetDate(days) {
        const checkDayOfWeek = date => {
            const getDay = new Date(date).getDay();
            const offset = (getDay === 6) ? 2 : (getDay === 0) ? 1 : 0;
            return date + offset * Release.ONE_DAY;
        }
        const today = new Date();
        let newDate = new Date(today.getTime()).setHours(10,0,0,0,);
        if (today.getHours() > 10) newDate += Release.ONE_DAY;
        newDate = checkDayOfWeek(newDate) + days.allCalendarDays * Release.ONE_DAY;
        return new Date(checkDayOfWeek(newDate));
    }

    checkDate(release) {
        const assignDate = release.releaseDate.getTime();
        const releaseDate = release.offsetDate(release).getTime();
        if (assignDate > releaseDate) {
            return console.log('We are on time for the release date');
        }
        const moveDate = Math.ceil((releaseDate - assignDate) / Release.ONE_DAY);
        return ('We are late, the release needs to be postponed to ' + moveDate + ' days');
    }

    assign(projectManager) {
        if (!(this.projectManager)) {
            this.projectManager = projectManager;
            Validator.validateExistenceArgument(projectManager, 'assign');
            projectManager.assign(this);
        }
    }
}
