export class InvalidValueError extends Error {
    constructor(message) {
        super();
        this.name = 'InvalidValueError';
        this.message = message;
    }
}

export default class Validator {
    static validateInputArgument(value, inputValue) {
        if (typeof (value) !== 'string') {
            throw new InvalidValueError (
                `'${inputValue}' parameter must be string, you entered ${typeof (value)}`
            )
        } else if (value === "") {
            throw new InvalidValueError (
                "The field is empty, fill in the field"
            )
        }
    }

    static validateExistenceArgument(value, inputValue) {
        if (value === undefined) {
            throw new InvalidValueError (
                `You did not enter the value of the argument in method '${inputValue}'`
            )
        } else if (typeof (value) !== 'object') {
            throw new InvalidValueError (
                `You need to pass an object to the method '${inputValue}', you entered ${typeof (value)}`
            )
        }
    }

    static validateFieldOfNumber(value, inputValue) {
        if (typeof (value) !== 'number') {
            throw new InvalidValueError (
                `'${inputValue}' parameter must be number, you entered ${typeof (value)}`
            );
        }
    }

    static validateArray(value, inputValue) {
        if (!Array.isArray(value)) {
            throw new InvalidValueError (
                `'${inputValue}' parameter must be array, you entered ${typeof (value)}`
            );
        }
    }

    static validateStatus(value, inputValue) {
        if (typeof (value) !== 'string') {
            throw new InvalidValueError(
                `'${inputValue}' must be string, you entered ${typeof (value)}`
            )
        }
    }

    static validateFieldOfId(value, inputValue) {
        if (value === 0) {
            throw new InvalidValueError(
                `'${inputValue}' parameter must be from one, you entered ${(value)}`
            )
        } else if (typeof (value) !== 'number') {
            throw new InvalidValueError(
                `'${inputValue}' parameter must be number, you entered ${typeof (value)}`
            )
        }
    }

    static validateNewDate(value, inputValue) {
        let valid = new Date(value).getTime();
        if (valid > 0) {
            return true;
        } else {
            throw new InvalidValueError(
                `'${inputValue}' entered incorrectly - you need to enter year '2000', month '00', day '00' in numbers`
            )
        }
    }
}
