import Validator from "./validator.js";
let idCount = 1;

export default class Task {
    constructor(name, description, estimate) {
        Validator.validateInputArgument(name, 'task name');
        Validator.validateInputArgument(description, 'description');
        Validator.validateFieldOfId(idCount, 'ID');
        Validator.validateStatus(Task.STATUS.statusOpen, 'statusOpen');
        Validator.validateStatus(Task.STATUS.statusInDev, 'statusInDev');
        Validator.validateStatus(Task.STATUS.statusFixed, 'statusFixed');

        this.ID = idCount++;
        this.name = name;
        this.description = description;
        this.status = Task.STATUS.statusOpen;
        this.estimate = estimate;
        this.loggedWork = 0;
    }

    static get STATUS() {
        return {
            statusOpen: "open",
            statusInDev: "in dev",
            statusFixed: "fixed"
        }
    }

    static get RISK_FACTOR() {
        return 1.3;
    }

    logged(value, done = '') {
        Validator.validateFieldOfNumber(value, 'logged value');
        if (this.status === Task.STATUS.statusInDev) this.loggedWork += value;
        if (done === Task.STATUS.statusFixed) this.status = Task.STATUS.statusFixed;
    }

    get remainTime() {
        Validator.validateFieldOfNumber(this.estimate, 'estimate');
        return (this.status === Task.STATUS.statusFixed) ? 0 : (this.estimate * Task.RISK_FACTOR - this.loggedWork);
    }

    assign(developer) {
        if (this.status === Task.STATUS.statusOpen) {
            Validator.validateExistenceArgument(developer, 'assign');
            this.assignee = developer;
            this.status = Task.STATUS.statusInDev;
            developer.assignedTasks.push(this);
        }
    }
}

