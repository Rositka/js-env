import Person from './person.js';
import Validator from "./validator.js";

class ProjectManager extends Person {
    constructor(firstName, lastName) {
        Validator.validateInputArgument(firstName, 'firstName');
        Validator.validateInputArgument(lastName, 'lastName');

        super(firstName, lastName);
        this.assignedReleases = [];
    }

    get fullName() {
        return this.firstName + " " + this.lastName;
    }

    assign(release) {
        if (!(release.projectManager)) {
            Validator.validateExistenceArgument(release, 'assign');
            release.assign(this);
            this.assignedReleases.push(release);
        }
    }
}

export default ProjectManager;

