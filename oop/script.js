import Developer from './developer.js';
import ProjectManager from './project-manager.js';
import Task from './task.js';
import Release from './release.js';

try {
    const dev1 = new Developer("Ivan", "Petrov");
    //const dev2 = new Developer("Ivan", "");
    const pm1 = new ProjectManager("Lena", "Smith");
    const task1 = new Task("exercises", "take new feature", 3600);
    const task2 = new Task("exercises2", "take new feature2", 6300);
    //const task3 = new Task("exercises3", "take new feature2", '1254');
    dev1.assign(task1);
    dev1.assign(task2);
    task1.logged(120);
    //task1.logged({});
    task1.logged(120, 'fixed');
    const release1 = new Release("release1", "new release1", [task1, task2]);
    //const release2 = new Release("release2", "new release2", {task1});
    console.log(release1);
    release1.addTasks(task1);
    pm1.assign(release1);
    release1.assignRelease(new Date(2021, 0, 15, 10, 0));
    console.log(release1.releaseDate);
    console.log(release1.offsetDate(release1));
    console.log(release1.checkDate(release1));
} catch (e) {
    console.error('Something went wrong!', e);
}





