const schengen = 1;
const euroZone = 2;
const euroUnion = 4;

const AUSTRIA = (8 << 13) + (83 << 3) + euroUnion + euroZone + schengen;
const BRITAIN = (66 << 13) + (243 << 3) + euroUnion;
const HUNGARY = (9 << 13) + (93 << 3) + euroUnion + schengen;
const DENMARK = (5 << 13) + (43 << 3) + euroUnion + schengen;
const CYPRUS = (1 << 13) + (9 << 3) + euroUnion + euroZone;
const NORWAY = (5 << 13) + (385 << 3) + schengen;
const POLAND= (38 << 13) + (312 << 3) + euroUnion + schengen;
const SLOVENIA = (2 << 13) + (20 << 3) + euroUnion + euroZone + schengen;
const CZECH = (10 << 13) + (78 << 3) + euroUnion + schengen;
const SWEDEN = (10 << 13) + (450 << 3) + schengen;
const MONACO = (1 << 13) + (1 << 3) + euroZone;

//const countries = [AUSTRIA, BRITAIN, HUNGARY, DENMARK, CYPRUS, NORWAY, POLAND, SLOVENIA, CZECH, SWEDEN, MONACO];
const countries = [["AUSTRIA",AUSTRIA], ["BRITAIN", BRITAIN], ["HUNGARY", HUNGARY],
                    ["DENMARK", DENMARK], ["CYPRUS", CYPRUS], ["NORWAY", NORWAY],
                    ["POLAND", POLAND], ["SLOVENIA", SLOVENIA], ["CZECH", CZECH],
                    ["SWEDEN", SWEDEN], ["MONACO", MONACO]];

const isEuroZone = (item) => Boolean(item & euroZone);
const isEuroUnion = (item) => Boolean(item & euroUnion);
const isSchengen = (item) => Boolean(item & schengen);
const popul = (item) => item >> 13;
const area = (item) => (item ^ popul(item) << 13) >> 3;
const density = (item) => popul(item) / area(item) * 1000;


const resultA = countries.filter(item => isEuroZone(item[1]) & !isEuroUnion(item[1]));
const resultB = countries.filter(item => isSchengen(item[1]) & !isEuroZone(item[1]) &
                popul(item[1]) > 20 & area(item[1]) < 200);
const resultC = countries.filter(item => density(item[1]) > 200 & density(item[1]) < 400);


//console.log(AUSTRIA.toString(2));
console.log("Population in millions " + popul(AUSTRIA));
console.log("Area in thousands of square kilometers " + area(AUSTRIA));
console.log(resultA[0]);
console.log(resultB);
console.log(resultC[0]);